# How to run #
Just like any Laravel APP:

1. Clone the repository
2. Run `composer install`
3. Make a copy of `.env.example` to `.env` and configure your database
4. Run `php artisan key:generate`
5. Run `php artisan migrate`
6. Run `php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\JWTAuthServiceProvider"`
7. Run `php artisan jwt:generate`
8. That's it! You're good to go.