<!doctype html>
<html lang="pt-BR" ng-app="app">
    <head>
        <base href="/">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <title>Lightning Talks</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900,400italic,100italic,300italic,500italic,700italic,900italic" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css">
        <link href="app/css/app.css" rel="stylesheet" type="text/css">
        <!-- AngularJS -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-animate.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-route.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-resource.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.10/ngStorage.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-moment/1.0.0-beta.3/angular-moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script src="app/js/app.js"></script>
    </head>
    <body ng-controller="mainCtrl" ng-init="getAuthenticatedUser()">
        <div class="container">
            <!-- Header -->
            <div ng-include="'app/view/header.html'"></div>
            <!-- Lightning Talks -->
            <div class="content">
                <div ng-view></div>
            </div>
        </div>
    </body>
</html>
