var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Compile things inside our Angular app folder.
elixir.config.publicPath = 'public/app';

elixir(function (mix) {
    mix.sass('app.scss')
        .scripts(['app.js', 'routes.js', 'controllers', 'directives', 'services', 'values'], elixir.config.publicPath + '/js/app.js');
});
