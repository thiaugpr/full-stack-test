app.controller('mainCtrl', ['$scope', '$location', '$localStorage', '$rootScope', '$route', 'Auth',
    function ($scope, $location, $localStorage, $rootScope, $route, Auth) {
        /**
         * Query the authenticated user by the Authorization token from the header.
         *
         * @param user {object} If provided, it won't query from database, but take this one.
         * @returns {null}
         */
        $scope.getAuthenticatedUser = function (user) {
            if (user) {
                $scope.authenticatedUser = user;
            } else {
                if (typeof $localStorage.token === 'undefined') {
                    return null;
                }
                Auth.refreshToken(function (response) {
                    $localStorage.token = response.token;
                    Auth.getByToken(function (user) {
                        $scope.authenticatedUser = user;
                    });
                })
            }
        };

        $scope.logout = function (showMessage) {
            showMessage = (typeof showMessage !== 'undefined') ? showMessage : true;
            delete $localStorage.token;
            $scope.authenticatedUser = null;
            $location.path('/');
            $route.reload();
            if (showMessage)
                swal("Logged out!", "You're now logged out!", "success");
        };

        $rootScope.$on('unauthorized', function () {
            $scope.logout(false);
        });
    }
]);