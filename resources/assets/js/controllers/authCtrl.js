app.controller('authCtrl', ['$scope', 'Auth', '$localStorage', '$location',
    function ($scope, Auth, $localStorage, $location) {
        $scope.login = function (credentials) {
            var user = new Auth(credentials);
            user.$login(function (user) {
                $localStorage.token = user.token;
                $scope.getAuthenticatedUser(user);
                $location.path('/');
                swal('Logged in!', 'Welcome back, ' + user.username + '!', 'success');
            }, function (err) {
                if (err.status = 422) {
                    $scope.credentials.password = '';
                    swal('Oops!', err.data.message, 'warning');
                } else {
                    swal('Error!', 'Our system is unstable, please try again later', 'error');
                }
            });
        };

        $scope.create = function (newUser) {
            var auth = new Auth({
                username: newUser.username,
                email: newUser.email,
                password: newUser.password,
                password_confirmation: newUser.passwordConfirmation
            });
            auth.$save(function (response) {
                if (response.success) {
                    $localStorage.token = response.data.token;
                    $scope.getAuthenticatedUser(response.data);
                    $location.path('/');
                    swal('Success!', 'Congratulations! Your account has been created and you\'re logged in!', 'success');
                } else {
                    swal('Oops!', response.message, 'info');
                }
            }, function (err) {
                swal('Error!', 'Something went wrong, please try again later', 'error');
            });
        };

        $scope.findOne = function () {
            var splitPath = $location.path().split('/');
            var userId = splitPath[splitPath.length - 1];
            $scope.user = User.get({userId: userId});
        };
    }
]);