app.controller('votesCtrl', ['$scope', '$location', '$route', 'Vote', function ($scope, $location, $route, Vote) {
    $scope.voteUp = function (lightningTalk) {
        var vote = new Vote({
            lightning_talk_id: lightningTalk.id
        });
        vote.$save(function (res) {
            if (res.success === true) {
                $route.reload();
            }
        });
    }
}]);