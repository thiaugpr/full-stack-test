app.controller('lightningTalksCtrl', ['$scope', '$location', '$rootScope', 'LightningTalk', 'lightningTalks', function ($scope, $location, $rootScope, LightningTalk, lightningTalks) {
    // Get the variable from "resolve"
    $scope.lightningTalks = lightningTalks;

    if ($location.path() === '/') {
        $scope.order = ['-votes_count', '-created_at']
    } else {
        $scope.order = ['-created_at', '-votes_count']
    }

    $scope.addLightningTalk = function (newLightningTalk) {
        var lightningTalk = new LightningTalk(newLightningTalk);
        lightningTalk.$save(function (response) {
            if (response.success === true) {
                $location.path('/');
                swal("Success!", "Your lightning talk is now visible to all users.", "success");
            } else {
                swal('Error!', 'There was an error, please try again later.', 'error');
            }
        }, function (err) {
            if (err.data.error == 'token_not_provided') {
                $location.path('/');
                swal('Error!', 'You\'re not logged in.', 'error');
            } else {
                swal('Error!', 'There was an error, please try again later.', 'error');
            }
        });
    }
}]);