app.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider.when('/', {
        templateUrl: 'app/view/show-lts.html',
        controller: 'lightningTalksCtrl',
        resolve: {
            lightningTalks: ['LightningTalk', function (LightningTalk) {
                return LightningTalk.query().$promise.then(function (data) {
                    return data;
                });
            }]
        }
    }).when('/newest', {
        templateUrl: 'app/view/show-lts.html',
        controller: 'lightningTalksCtrl',
        resolve: {
            lightningTalks: ['LightningTalk', function (LightningTalk) {
                return LightningTalk.query().$promise.then(function (data) {
                    return data;
                });
            }]
        }
    }).when('/my-contribution', {
        templateUrl: 'app/view/my-contribution.html',
        controller: 'lightningTalksCtrl',
        resolve: {
            lightningTalks: ['LightningTalk', function (LightningTalk) {
                return LightningTalk.getByUser().$promise.then(function (data) {
                    return data;
                });
            }]
        }
    }).when('/new', {
        templateUrl: 'app/view/new-lt.html',
        controller: 'lightningTalksCtrl',
        resolve: {
            lightningTalks: function () {
                return null;
            }
        }
    }).when('/login', {
        templateUrl: 'app/view/login.html',
        controller: 'authCtrl'
    }).otherwise({
        redirectTo: '/'
    });

    $locationProvider.html5Mode(true);

    $httpProvider.interceptors.push(['$rootScope', '$q', '$localStorage', '$injector',
        function ($rootScope, $q, $localStorage) {
            return {
                request: function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                response: function (res) {
                    return res || $q.when(res);
                },
                responseError: function (response) {
                    if (response.status === 401 || response.status === 400) {
                        $rootScope.$broadcast('unauthorized');
                    }
                    return $q.reject(response);
                }
            };
        }
    ]);
}]);