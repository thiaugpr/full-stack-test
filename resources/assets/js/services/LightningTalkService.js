app.factory('LightningTalk', ['$resource', 'config',
    function ($resource, config) {
        return $resource(config.baseUrl + '/lts/:ltId', {
            ltId: '@id'
        }, {
            update: {
                method: 'PUT'
            },
            getByUser: {
                method: 'GET',
                url: config.baseUrl + '/lts/my',
                isArray: true
            }
        });
    }
]);