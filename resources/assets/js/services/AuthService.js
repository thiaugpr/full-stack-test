app.factory('Auth', ['$resource', 'config',
    function ($resource, config) {
        return $resource(config.baseUrl + '/auth/:userId', {
            userId: '@id'
        }, {
            update: {
                method: 'PUT'
            },
            login: {
                method: 'POST',
                url: config.baseUrl + '/auth/login'
            },
            getByToken: {
                method: 'GET',
                url: config.baseUrl + '/auth/getByToken'
            },
            refreshToken: {
                method: 'GET',
                url: config.baseUrl + '/auth/refreshToken'
            }
        });
    }
]);