app.factory('Vote', ['$resource', 'config',
    function ($resource, config) {
        return $resource(config.baseUrl + '/votes');
    }
]);