<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{
    private $jwtAuth;
    private $req;
    private $user;

    function __construct(User $user, JWTAuth $jwtAuth)
    {
        $this->user = $user;
        $this->jwtAuth = $jwtAuth;
    }

    /**
     * Log a user in.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $user = $this->user->authenticate($request->input('username'), $request->input('password'));
        if(!$user) {
            return response()->json([
                'success' => false,
                'code'    => null,
                'message' => 'Login failed. Wrong username/password.',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user['token'] = $this->jwtAuth->fromUser($user);

        return response()->json($user, Response::HTTP_OK);
    }

    /**
     * Get a user by the token from the header.
     *
     * @return Response
     */
    public function getByToken()
    {
        return $this->jwtAuth->parseToken()->authenticate();
    }

    /**
     * Refresh token.
     *
     * @return Response
     */
    public function refreshToken()
    {
        $token = $this->jwtAuth->refresh($this->jwtAuth->getToken());

        return response()->json(['token' => $token]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()) {
            return response()->json(['success' => false, 'data' => null, 'message' => $validator->messages()->first()], 200);
        }

        $user = $this->create($request->all());
        if(!$user->save()) {
            abort(500, 'Could not save user.');
        }
        $user['token'] = $this->jwtAuth->fromUser($user);

        return ['success' => true, 'data' => $user];
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'email'    => 'email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return new User([
            'username' => $data['username'],
            'email'    => (isset($data['email']) && $data['email'] != '') ? $data['email'] : null,
            'password' => bcrypt($data['password']),
        ]);
    }
}