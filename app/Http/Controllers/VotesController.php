<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VotesController extends Controller
{
    public function store(Request $request)
    {
        try {
            $vote = Vote::voteUp(Auth::user()->id, $request->lightning_talk_id);

            return response()->json(['success' => true, 'data' => $vote]);
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'data' => null, 'message' => $e->getMessage()]);
        }
    }
}
