<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\LightningTalk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LightningTalksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $lightning_talks = LightningTalk::with('user', 'votes')->get();
            $user = JWTAuth::parseToken()->authenticate();
        } catch(JWTException $e) {
            $user = null;
        } catch(\Exception $e) {
            return response()->json([['success' => false, 'data' => null, 'message' => $e->getMessage()]]);
        }
        foreach($lightning_talks as $lightning_talk) {
            if($user !== null) {
                foreach($lightning_talk->votes as $vote) {
                    if($vote->user_id == $user->id) {
                        $lightning_talk->vote_disabled = true;
                        break;
                    }
                }
            } else {
                $lightning_talk->vote_disabled = true;
            }
            $lightning_talk->votes_count = count($lightning_talk->votes);
            unset($lightning_talk->votes);
        }

        return response()->json($lightning_talks);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function getAllbyLoggedInUser()
    {
        return response()->json(Auth::user()->lightningTalks()->with('user', 'votes')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        try {
            $lightningTalk = Auth::user()->lightningTalks()->create([
                'title'       => $request->title,
                'description' => $request->description,
            ]);

            return response()->json(['success' => true, 'data' => $lightningTalk, 'message' => null]);
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'data' => null, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public
    function show($lt)
    {
        return response()->json(LightningTalk::with('user', 'votes')->find($lt));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        try {
            $lightningTalk = LightningTalk::find($id);
            $lightningTalk->title = $request->title;
            if(isset($request->description)) {
                $lightningTalk->description = $request->description;
            }
            $lightningTalk->save();

            return response()->json(['success' => true, 'data' => $lightningTalk, 'message' => null]);
        } catch(\Exception $e) {
            return response()->json(['success' => false, 'data' => null, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        LightningTalk::destroy($id);
    }
}
