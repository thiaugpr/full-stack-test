<?php

/**
 * Web requests
 * Will render angularjs index page
 */
Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('index');
    });
});


/**
 * API requests
 */
Route::group(['prefix' => 'api/v1/', 'middleware' => ['api']], function () {
    /**
     * Public
     * Methods that are available for all users
     */
    Route::post('auth/login', 'AuthController@login');
    Route::get('auth/getByToken', 'AuthController@getByToken');
    Route::get('auth/refreshToken', 'AuthController@refreshToken', ['middleware' => 'jwt.refresh']);

    Route::resource('auth', 'AuthController', ['except' => [
        'create', 'destroy', 'index',
    ]]);
    Route::resource('lts', 'LightningTalksController', ['only' => [
        'index',
    ]]);

    /**
     * Authenticated Users
     * Methods that are available only for authenticated users.
     */
    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::resource('lts', 'LightningTalksController', ['only' => [
            'store', 'update', 'destroy',
        ]]);
        Route::get('lts/my', 'LightningTalksController@getAllbyLoggedInUser');

        Route::resource('votes', 'VotesController', ['only' => [
            'store',
        ]]);
    });
});

// Catch all undefined routes. Always gotta stay at the bottom since order of routes matters.
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('index');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');
