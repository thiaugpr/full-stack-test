<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    protected $table = 'votes';

    protected $fillable = [
        'lightning_talk_id',
        'user_id',
    ];

    /**
     * Telling laravel to not set updated_at column
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param integer $user_id
     * @param integer $lightning_talk_id
     * @return \App\Vote
     * @throws \Exception
     */
    public static function voteUp($user_id, $lightning_talk_id)
    {
        if(self::where(['lightning_talk_id' => $lightning_talk_id, 'user_id' => $user_id])->count() < 1) {
            return self::create([
                'lightning_talk_id' => $lightning_talk_id,
                'user_id'           => $user_id,
            ]);
        } else {
            throw new \Exception('User already increased this Lightning Talk point.');
        }
    }

    public function lightningTalk()
    {
        return $this->belongsTo(LightningTalk::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Since we disabled the timestamps, we override the boot method to set the created_at column
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->setCreatedAt($model->freshTimestamp());
        });
    }
}
